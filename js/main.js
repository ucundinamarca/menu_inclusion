
var ST = "#Stage_";
var S = "Stage_";
ivo.info({
    title: "Menú",
    autor: "Edilson Laverde Molina",
    date:  "10/03/2021",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var audios=[
    {url:"sonidos/click.mp3",      name:"clic"  }
];
var textos=[
    {t:`
    <div>
        
        <p>Apreciado participante le damos la bienvenida a este diplomado que ha sido diseñado y puesto a disposición para usted como estudiante con la intención de contribuir a la construcción de país.</p>
        <p>Este diplomado lo invita a vivenciar la educación inclusiva entendida como oportunidad de aprendizaje, participación y fortalecimiento que promueve el respeto a la diversidad y la equidad.</p>
        <p>Este proceso de formación apuesta al Modelo Educativo Digital Transmoderno de nuestra universidad, el cual asume una postura denominada construcción dialógica y formativa que pretende educar sujetos libres que a partir del diálogo y la interacción con otros fortalezcan su identidad y se hagan más fuertes como seres humanos sociales.</p>
        <p>Dicho esto, la educación inclusiva se caracteriza por promover la participación, el respeto a la diversidad, establecer relaciones con interacción dialógica y potenciando aprendizajes a partir de la interculturalidad, de generar procesos de atención educativa con equidad y calidad como conceptos dinámicos que permiten el mejoramiento continuo de la educación.</p>
        <p>Finalmente, con el proceso formativo aquí propuesto, se busca dar respuesta pertinente a la necesidad de implementar estrategias inclusivas desde las prácticas educativas en nuestra institución, a través de acciones intencionadas dirigidas al saber y al ser.</p>
    </div>
    `},
    {t:`
    <div>
        
        <p>El paradigma de la educación inclusiva a escala mundial da cuenta de modificaciones lingüísticas y estructurales en el orden, político, económico, social, cultural y personal. Forjando así estrategias que aportan a los procesos de formación de las comunidades, y trasciende más allá de la atención específica de grupos priorizados (MEN, 2013). En este orden, la educación inclusiva tiene por principios la integralidad, es decir, amplía la dimensión de estrategias y líneas de acción que se ven reflejadas en las estrategias pedagógicas flexibles, por su capacidad de adaptabilidad para responder a la diversidad cultural y social.</p>
        <p>Los temas que se abordan en el presente curso, están relacionados con eventos históricos asociados al reconocimiento de la diversidad y el desarrollo del concepto de inclusión, destacando las principales diferencias entre exclusión, segregación, integración e inclusión, prácticas educativas incluyentes, conceptualizaciones de educación para todos, atención educativa basada en la rehabilitación, enfoque diferencial, transición de individuo - sujeto, necesidades educativas diversas y el reconocimiento de poblaciones de especial protección constitucional.</p>
    </div>
    `},
    {t:`
    <div>
        
        <p><b>REA GENERAL</b><br>El paradigma de la educación inclusiva a escala mundial da cuenta de modificaciones lingüísticas y estructurales en el orden, político, económico, social, cultural y personal. Forjando así estrategias que aportan a los procesos de formación de las comunidades, y trasciende más allá de la atención específica de grupos priorizados (MEN, 2013). En este orden, la educación inclusiva tiene por principios la integralidad, es decir, amplía la dimensión de estrategias y líneas de acción que se ven reflejadas en las estrategias pedagógicas flexibles, por su capacidad de adaptabilidad para responder a la diversidad cultural y social.</p>
        <p><b>REA 1:</b>Implementar al interior de nuestro contexto universitario los fundamentos de la educación inclusiva mediante la aplicación de los lineamientos establecidos para la misma.</p>
        <p><b>REA 2:</b>Interpretar el fundamento legal y normativo a nivel internacional y nacional sobre la atención educativa a través de situaciones problémicas contextualizadas con nuestra realidad.</p>
        <p><b>REA 3:</b>Aplicar en el quehacer educativo de nuestra universidad a los tres principios del diseño universal para el aprendizaje –DUA- a través de situaciones problémicas ligadas a nuestra realidad.</p>
    </div>
    `},
    {t:`
    <div>
        
        <p>La educación como derecho implica garantizar y asegurar el servicio de atención educativa, respetando y promoviendo acciones de participación y aprendizaje para todos. En este orden, la metodología de este curso es flexible, responde a problemas de la vida cotidiana, y a hacer realidad la inclusión, considerando a los sujetos con necesidades educativas diversas y de especial protección constitucional.</p>
        <p>El presente curso virtual, se divide en tres REA, se llevará a cabo en 8 semanas, tendrá tres encuentros sincrónicos así: Uno durante la semana 1 para dar inicio al proceso; el segundo, en la semana 3 con el fin de explicar el marco legal de la educación inclusiva en nuestro país, y el último, para explicar el Diseño Universal para el Aprendizaje –DUA- como herramienta para garantizar la educación inclusiva. Adicionalmente, el diplomado cuenta con el acompañamiento de un tutor. Las actividades son prácticas y para su desarrollo se motiva el uso del pensamiento crítico, el análisis, la reflexión constructiva y dialogante, con el fin de asegurar y facilitar la construcción de conocimientos para la vida.</p>
        <p>Con la intención de llevar a feliz término éste proceso formativo, lo invitamos a trabajar de manera responsable y comprometida, pues solo así podrá alcanzar los resultados de aprendizaje propuestos.</p>
        <p>Recuerde que la interacción con los recursos y compañeros es indispensable para potenciar la construcción de conocimientos, por lo cual lo invitamos a hacer uso del foro, <b>pregúntele a su tutor,</b> allí encontrará respuestas a sus inquietudes e interrogantes.</p>
        <p>No olvide prestar especial atención a las instrucciones que se dan a lo largo del proceso. Esperamos que este trabajo redunde en beneficios para su vida y mejore su ámbito laboral y profesional.</p>
    </div>
    `},
    {t:`
    <div>
        
        <p>La evaluación en este Curso es dialógica y permanente. Responde a un proceso flexible en el que se han planteado actividades en las que la interacción, la resolución de problemas, la práctica y la realimentación permanente contribuyen a la reflexión y la construcción de conocimientos y por lo tanto, al alcance de los REA propuestos.</p>
        <p>Las actividades desarrolladas a lo largo del proceso permiten que los estudiantes se autoevalúen de manera permanente, logrando de éste modo un aprendizaje para la vida, que los conduzca a la creación y fortalecimiento de nuevos aprendizajes transversales a su rol profesional.</p>
        <p>Siendo conocedores de la importancia de los aspectos aquí estudiados y advirtiendo, que éste es un curso que contribuye positivamente a su formación humana y profesional, se ha contemplado un tiempo prudencial para su realización.</p>
        <p>Recuerde que el curso se da por aprobado al superar el 80% de las actividades propuestas.</p>
    </div>
    `}
]
var index=0;
function main(sym) {
var t=null;
udec =  ivo.structure({
        created:function(){
            t=this;
            //precarga audios//
            ivo.load_audio(audios,onComplete=function(){
               ivo(ST+"preload").hide();
               t.events();
               t.animation();
               intro.play();
            });
        },
        methods: {
            events:function(){
                var t=this;
             
                $(ST+"b1").on({
                    "click":function(){
                        ivo(".labels").hide();
                        ivo(ST+"label1").show();
                        ivo(ST+"texto").html(textos[0].t);
                        ivo.play("clic");
                        pop.play();
                    }
                });
                $(ST+"b2").on({
                    "click":function(){
                        ivo(".labels").hide();
                        ivo(ST+"label2").show();
                        ivo(ST+"texto").html(textos[1].t);
                        ivo.play("clic");
                        pop.play();
                    }
                });
                $(ST+"b3").on({
                    "click":function(){
                        ivo(".labels").hide();
                        ivo(ST+"label3").show();
                        ivo(ST+"texto").html(textos[2].t);
                        ivo.play("clic");
                        pop.play();
                    }
                });
                $(ST+"b4").on({
                    "click":function(){
                        ivo(".labels").hide();
                        ivo(ST+"label4").show();
                        ivo(ST+"texto").html(textos[3].t);
                        ivo.play("clic");
                        pop.play();
                    }
                });
                $(ST+"b5").on({
                    "click":function(){
                        ivo(".labels").hide();
                        ivo(ST+"label5").show();
                        ivo(ST+"texto").html(textos[4].t);
                        ivo.play("clic");
                        pop.play();
                    }
                });
                $(ST+"close").on("click",function(){
                    pop.reverse();
                    ivo.play("clic");
                });

            },
            animation:function(){
                pop = new TimelineMax();
                pop.append(TweenMax.from(ST+"pop_up", .4,   {opacity:0,y:900}), 0); 
                pop.append(TweenMax.from(ST+"close", .8,    {opacity:0, rotation:900, scale:0}), 0);
                pop.stop();

                intro = new TimelineMax();
                intro.append(TweenMax.from(ST+"lab2", .4,      {opacity:0, x:-200}), 0); 
                intro.append(TweenMax.from(ST+"label_p", .4,   {opacity:0, y:900}), 0); 
                intro.append(TweenMax.from(ST+"avatares", .8,  {scaleY:0 }), 0);
                intro.append(TweenMax.from(ST+"lapiz", .8,     {y:400, opacity:0}), 0);
                intro.append(TweenMax.from(ST+"ramas", .8,     {y:140, opacity:0}), 0);
                intro.append(TweenMax.from(ST+"hojas2", .8,    {opacity:0}), 0);
                intro.append(TweenMax.from(ST+"hojas", .8,    {opacity:0}), 0);
                intro.stop();

            }
        }
 });
}
