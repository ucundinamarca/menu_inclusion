/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'fondo',
                            type: 'image',
                            rect: ['0px', '0px', '1020px', '783px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"fondo.jpg",'0px','0px']
                        },
                        {
                            id: 'lab2',
                            type: 'image',
                            rect: ['0px', '5px', '240px', '93px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"Recurso_7.png",'0px','0px']
                        },
                        {
                            id: 'lapiz',
                            type: 'image',
                            rect: ['479px', '257px', '79px', '347px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"Recurso_15.png",'0px','0px']
                        },
                        {
                            id: 'hojas',
                            symbolName: 'hojas',
                            type: 'rect',
                            rect: ['311', '69', '473', '356', 'auto', 'auto']
                        },
                        {
                            id: 'hojas2',
                            type: 'image',
                            rect: ['277px', '38px', '506px', '383px', 'auto', 'auto'],
                            opacity: '0.78620686818813',
                            fill: ["rgba(0,0,0,0)",im+"Recurso_16.png",'0px','0px']
                        },
                        {
                            id: 'ramas',
                            type: 'image',
                            rect: ['310px', '104px', '406px', '363px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"Recurso_17.png",'0px','0px']
                        },
                        {
                            id: 'avatares',
                            symbolName: 'avatares',
                            type: 'rect',
                            rect: ['296', '342', '468', '324', 'auto', 'auto']
                        },
                        {
                            id: 'label_p',
                            type: 'image',
                            rect: ['284px', '652px', '494px', '88px', 'auto', 'auto'],
                            fill: ["rgba(0,0,0,0)",im+"Recurso_19.png",'0px','0px']
                        },
                        {
                            id: 'b1',
                            type: 'rect',
                            rect: ['310px', '337px', '99px', '93px', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(255,255,255,0.00)"],
                            stroke: [0,"rgba(0,0,0,1)","none"]
                        },
                        {
                            id: 'b2',
                            type: 'rect',
                            rect: ['323px', '76px', '149px', '153px', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(255,255,255,0.00)"],
                            stroke: [0,"rgba(0,0,0,1)","none"]
                        },
                        {
                            id: 'b3',
                            type: 'rect',
                            rect: ['479px', '152px', '116px', '111px', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(255,255,255,0.00)"],
                            stroke: [0,"rgba(0,0,0,1)","none"]
                        },
                        {
                            id: 'b4',
                            type: 'rect',
                            rect: ['679px', '69px', '99px', '92px', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(255,255,255,0.00)"],
                            stroke: [0,"rgba(0,0,0,1)","none"]
                        },
                        {
                            id: 'b5',
                            type: 'rect',
                            rect: ['630px', '236px', '123px', '118px', 'auto', 'auto'],
                            cursor: 'pointer',
                            fill: ["rgba(255,255,255,0.00)"],
                            stroke: [0,"rgba(0,0,0,1)","none"]
                        },
                        {
                            id: 'pop_up',
                            type: 'group',
                            rect: ['0', '0', '1020', '783', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-pantalla2',
                                type: 'image',
                                rect: ['0px', '0px', '1020px', '783px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"fondo-pantalla2.jpg",'0px','0px']
                            },
                            {
                                id: 'label',
                                type: 'image',
                                rect: ['0px', '5px', '240px', '93px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_7.png",'0px','0px']
                            },
                            {
                                id: 'texto',
                                type: 'text',
                                rect: ['25px', '228px', '824px', '514px', 'auto', 'auto'],
                                text: "<p style=\"margin: 0px;\">​</p>",
                                font: ['Arial, Helvetica, sans-serif', [24, ""], "rgba(0,0,0,1)", "normal", "none", "", "break-word", "normal"]
                            },
                            {
                                id: 'label1',
                                type: 'image',
                                rect: ['0px', '104px', '494px', '88px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_18.png",'0px','0px'],
                                userClass: "labels"
                            },
                            {
                                id: 'label2',
                                type: 'image',
                                rect: ['0px', '104px', '494px', '88px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_4.png",'0px','0px'],
                                userClass: "labels"
                            },
                            {
                                id: 'label4',
                                type: 'image',
                                rect: ['0px', '104px', '494px', '88px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_2.png",'0px','0px'],
                                userClass: "labels"
                            },
                            {
                                id: 'label3',
                                type: 'image',
                                rect: ['0px', '104px', '592px', '88px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_3.png",'0px','0px'],
                                userClass: "labels"
                            },
                            {
                                id: 'label5',
                                type: 'image',
                                rect: ['0px', '104px', '462px', '88px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_1.png",'0px','0px'],
                                userClass: "labels"
                            },
                            {
                                id: 'close',
                                type: 'image',
                                rect: ['880px', '58px', '80px', '79px', 'auto', 'auto'],
                                cursor: 'pointer',
                                fill: ["rgba(0,0,0,0)",im+"Recurso_5.png",'0px','0px']
                            }]
                        },
                        {
                            id: 'preload',
                            type: 'group',
                            rect: ['0', '0', '1024', '783px', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Rectangle3',
                                type: 'rect',
                                rect: ['0px', '0px', '1024px', '783px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'logo',
                                type: 'image',
                                rect: ['462px', '293px', '100px', '122px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"logo.svg",'0px','0px']
                            }]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1024px', '771px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 4500,
                    autoPlay: true,
                    data: [
                        [
                            "eid10337",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${avatares}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10458",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${avatares}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10459",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${avatares}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10460",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${avatares}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10461",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${avatares}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10462",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${avatares}",
                            [50,100],
                            [50,100],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ]
                    ]
                }
            },
            "hojas": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            rect: ['0px', '263px', '95px', '93px', 'auto', 'auto'],
                            id: 'btn1',
                            transform: [[], ['6']],
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', 'images/Recurso_12.png', '0px', '0px']
                        },
                        {
                            rect: ['23px', '10px', '143px', '142px', 'auto', 'auto'],
                            id: 'btn2',
                            transform: [[], ['6']],
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', 'images/Recurso_11.png', '0px', '0px']
                        },
                        {
                            rect: ['177px', '81px', '109px', '109px', 'auto', 'auto'],
                            id: 'btn3',
                            transform: [[], ['6']],
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', 'images/Recurso_8.png', '0px', '0px']
                        },
                        {
                            id: 'btn4',
                            type: 'image',
                            rect: ['369px', '0px', '104px', '93px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/Recurso_10.png', '0px', '0px']
                        },
                        {
                            rect: ['322px', '172px', '119px', '118px', 'auto', 'auto'],
                            id: 'btn5',
                            transform: [[], ['4']],
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', 'images/Recurso_9.png', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            isStage: 'true',
                            rect: [undefined, undefined, '473px', '356px']
                        }
                    }
                },
                timeline: {
                    duration: 4500,
                    autoPlay: true,
                    data: [
                        [
                            "eid10148",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${btn1}",
                            [75,19],
                            [82,30],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10463",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${btn1}",
                            [75,19],
                            [82,30],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10464",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${btn1}",
                            [75,19],
                            [82,30],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10465",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${btn1}",
                            [75,19],
                            [82,30],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10466",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${btn1}",
                            [75,19],
                            [82,30],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10467",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${btn1}",
                            [75,19],
                            [82,30],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10150",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${btn2}",
                            [37,47],
                            [43,68],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10468",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${btn2}",
                            [37,47],
                            [43,68],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10469",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${btn2}",
                            [37,47],
                            [43,68],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10470",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${btn2}",
                            [37,47],
                            [43,68],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10471",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${btn2}",
                            [37,47],
                            [43,68],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10472",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${btn2}",
                            [37,47],
                            [43,68],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid14",
                            "rotateZ",
                            500,
                            2000,
                            "linear",
                            "${btn2}",
                            '6deg',
                            '-6deg'
                        ],
                        [
                            "eid16",
                            "rotateZ",
                            2500,
                            2000,
                            "linear",
                            "${btn2}",
                            '-6deg',
                            '6deg'
                        ],
                        [
                            "eid10152",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${btn3}",
                            [35,18],
                            [37,49],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10473",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${btn3}",
                            [35,18],
                            [37,49],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10474",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${btn3}",
                            [35,18],
                            [37,49],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10475",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${btn3}",
                            [35,18],
                            [37,49],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10476",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${btn3}",
                            [35,18],
                            [37,49],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10477",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${btn3}",
                            [35,18],
                            [37,49],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid33",
                            "rotateZ",
                            250,
                            1750,
                            "linear",
                            "${btn5}",
                            '4deg',
                            '-4deg'
                        ],
                        [
                            "eid35",
                            "rotateZ",
                            2000,
                            1750,
                            "linear",
                            "${btn5}",
                            '-4deg',
                            '4deg'
                        ],
                        [
                            "eid5",
                            "rotateZ",
                            0,
                            2000,
                            "linear",
                            "${btn1}",
                            '6deg',
                            '-6deg'
                        ],
                        [
                            "eid8",
                            "rotateZ",
                            2000,
                            2000,
                            "linear",
                            "${btn1}",
                            '-6deg',
                            '6deg'
                        ],
                        [
                            "eid21",
                            "rotateZ",
                            250,
                            2000,
                            "linear",
                            "${btn3}",
                            '6deg',
                            '-6deg'
                        ],
                        [
                            "eid23",
                            "rotateZ",
                            2250,
                            1750,
                            "linear",
                            "${btn3}",
                            '-6deg',
                            '6deg'
                        ],
                        [
                            "eid10231",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${btn4}",
                            [32,55],
                            [32,55],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10478",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${btn4}",
                            [32,55],
                            [32,55],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10479",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${btn4}",
                            [32,55],
                            [32,55],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10480",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${btn4}",
                            [32,55],
                            [32,55],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10481",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${btn4}",
                            [32,55],
                            [32,55],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10482",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${btn4}",
                            [32,55],
                            [32,55],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10235",
                            "-webkit-transform-origin",
                            0,
                            0,
                            "linear",
                            "${btn5}",
                            [31,50],
                            [31,50],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10483",
                            "-moz-transform-origin",
                            0,
                            0,
                            "linear",
                            "${btn5}",
                            [31,50],
                            [31,50],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10484",
                            "-ms-transform-origin",
                            0,
                            0,
                            "linear",
                            "${btn5}",
                            [31,50],
                            [31,50],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10485",
                            "msTransformOrigin",
                            0,
                            0,
                            "linear",
                            "${btn5}",
                            [31,50],
                            [31,50],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10486",
                            "-o-transform-origin",
                            0,
                            0,
                            "linear",
                            "${btn5}",
                            [31,50],
                            [31,50],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid10487",
                            "transform-origin",
                            0,
                            0,
                            "linear",
                            "${btn5}",
                            [31,50],
                            [31,50],
                            {valueTemplate: '@@0@@% @@1@@%'}
                        ],
                        [
                            "eid26",
                            "rotateZ",
                            0,
                            2000,
                            "linear",
                            "${btn4}",
                            '0deg',
                            '-6deg'
                        ],
                        [
                            "eid28",
                            "rotateZ",
                            2000,
                            2000,
                            "linear",
                            "${btn4}",
                            '-6deg',
                            '0deg'
                        ]
                    ]
                }
            },
            "avatares": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'personajes',
                            type: 'group',
                            rect: ['0px', '0px', '468', '324', 'auto', 'auto'],
                            c: [
                            {
                                id: 'avatares',
                                type: 'image',
                                rect: ['0px', '0px', '468px', '324px', 'auto', 'auto'],
                                fill: ['rgba(0,0,0,0)', 'images/avatares.png', '0px', '0px']
                            }]
                        },
                        {
                            id: 'ojos1',
                            symbolName: 'ojos1',
                            rect: ['13', '176', '40', '24', 'auto', 'auto'],
                            type: 'rect'
                        },
                        {
                            id: 'ojos2',
                            symbolName: 'ojos2',
                            rect: ['82px', '175', '40', '24', 'auto', 'auto'],
                            type: 'rect'
                        },
                        {
                            id: 'ojos3',
                            symbolName: 'ojos2',
                            rect: ['151px', '74px', '40', '24', 'auto', 'auto'],
                            type: 'rect'
                        },
                        {
                            id: 'ojos4',
                            symbolName: 'ojos1',
                            rect: ['116px', '127px', '40', '24', 'auto', 'auto'],
                            type: 'rect'
                        },
                        {
                            id: 'ojos5',
                            symbolName: 'ojos1',
                            rect: ['120px', '21px', '40', '24', 'auto', 'auto'],
                            type: 'rect'
                        },
                        {
                            id: 'ojos6',
                            symbolName: 'ojos1',
                            rect: ['216px', '173px', '40', '24', 'auto', 'auto'],
                            type: 'rect'
                        },
                        {
                            id: 'ojos7',
                            symbolName: 'ojos1',
                            rect: ['352px', '173px', '40', '24', 'auto', 'auto'],
                            type: 'rect'
                        },
                        {
                            id: 'ojos8',
                            symbolName: 'ojos1',
                            rect: ['287px', '72px', '40', '24', 'auto', 'auto'],
                            type: 'rect'
                        },
                        {
                            id: 'ojos9',
                            symbolName: 'ojos2',
                            rect: ['283px', '175', '40', '24', 'auto', 'auto'],
                            type: 'rect'
                        },
                        {
                            id: 'ojos10',
                            symbolName: 'ojos2',
                            rect: ['187px', '18px', '40', '24', 'auto', 'auto'],
                            type: 'rect'
                        },
                        {
                            id: 'ojos11',
                            symbolName: 'ojos2',
                            rect: ['319px', '19px', '40', '24', 'auto', 'auto'],
                            type: 'rect'
                        },
                        {
                            id: 'ojos12',
                            symbolName: 'ojos2',
                            rect: ['417px', '173px', '40', '24', 'auto', 'auto'],
                            type: 'rect'
                        },
                        {
                            id: 'ojos13',
                            symbolName: 'ojos2',
                            rect: ['319px', '127px', '40', '24', 'auto', 'auto'],
                            type: 'rect'
                        },
                        {
                            id: 'ojos14',
                            symbolName: 'ojos2',
                            rect: ['147px', '174px', '40', '24', 'auto', 'auto'],
                            type: 'rect'
                        },
                        {
                            id: 'ojos15',
                            symbolName: 'ojos2',
                            rect: ['48px', '128px', '40', '24', 'auto', 'auto'],
                            type: 'rect'
                        },
                        {
                            id: 'ojos16',
                            symbolName: 'ojos16',
                            rect: ['179', '129', '40', '24', 'auto', 'auto'],
                            type: 'rect'
                        },
                        {
                            id: 'ojos17',
                            symbolName: 'ojos2',
                            rect: ['251px', '129px', '40', '24', 'auto', 'auto'],
                            type: 'rect'
                        },
                        {
                            id: 'ojos18',
                            symbolName: 'ojos16',
                            rect: ['387px', '125px', '40', '24', 'auto', 'auto'],
                            type: 'rect'
                        },
                        {
                            id: 'ojos19',
                            symbolName: 'ojos16',
                            rect: ['82px', '73px', '40', '24', 'auto', 'auto'],
                            type: 'rect'
                        },
                        {
                            id: 'ojos20',
                            symbolName: 'ojos16',
                            rect: ['218px', '72px', '40', '24', 'auto', 'auto'],
                            type: 'rect'
                        },
                        {
                            id: 'ojos21',
                            symbolName: 'ojos16',
                            rect: ['353px', '74px', '40', '24', 'auto', 'auto'],
                            type: 'rect'
                        },
                        {
                            id: 'ojos22',
                            symbolName: 'ojos2',
                            rect: ['255px', '22px', '40', '24', 'auto', 'auto'],
                            type: 'rect'
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            isStage: 'true',
                            rect: [undefined, undefined, '468px', '324px']
                        }
                    }
                },
                timeline: {
                    duration: 3000,
                    autoPlay: true,
                    data: [
                        [
                            "eid8212",
                            "top",
                            0,
                            0,
                            "linear",
                            "${ojos4}",
                            '127px',
                            '127px'
                        ],
                        [
                            "eid8089",
                            "left",
                            0,
                            0,
                            "linear",
                            "${ojos3}",
                            '151px',
                            '151px'
                        ],
                        [
                            "eid9410",
                            "left",
                            0,
                            0,
                            "linear",
                            "${ojos14}",
                            '147px',
                            '147px'
                        ],
                        [
                            "eid9695",
                            "top",
                            0,
                            0,
                            "linear",
                            "${ojos17}",
                            '129px',
                            '129px'
                        ],
                        [
                            "eid9736",
                            "top",
                            0,
                            0,
                            "linear",
                            "${ojos19}",
                            '73px',
                            '73px'
                        ],
                        [
                            "eid9837",
                            "top",
                            0,
                            0,
                            "linear",
                            "${ojos21}",
                            '74px',
                            '74px'
                        ],
                        [
                            "eid9536",
                            "left",
                            0,
                            0,
                            "linear",
                            "${ojos15}",
                            '48px',
                            '48px'
                        ],
                        [
                            "eid8654",
                            "top",
                            0,
                            0,
                            "linear",
                            "${ojos8}",
                            '72px',
                            '72px'
                        ],
                        [
                            "eid9276",
                            "top",
                            0,
                            0,
                            "linear",
                            "${ojos12}",
                            '173px',
                            '173px'
                        ],
                        [
                            "eid9275",
                            "left",
                            0,
                            0,
                            "linear",
                            "${ojos12}",
                            '417px',
                            '417px'
                        ],
                        [
                            "eid8090",
                            "top",
                            0,
                            0,
                            "linear",
                            "${ojos3}",
                            '74px',
                            '74px'
                        ],
                        [
                            "eid9182",
                            "top",
                            0,
                            0,
                            "linear",
                            "${ojos11}",
                            '19px',
                            '19px'
                        ],
                        [
                            "eid8747",
                            "left",
                            0,
                            0,
                            "linear",
                            "${ojos7}",
                            '352px',
                            '352px'
                        ],
                        [
                            "eid9770",
                            "top",
                            0,
                            0,
                            "linear",
                            "${ojos20}",
                            '72px',
                            '72px'
                        ],
                        [
                            "eid9734",
                            "left",
                            0,
                            0,
                            "linear",
                            "${ojos19}",
                            '82px',
                            '82px'
                        ],
                        [
                            "eid9535",
                            "top",
                            0,
                            0,
                            "linear",
                            "${ojos15}",
                            '128px',
                            '128px'
                        ],
                        [
                            "eid9408",
                            "top",
                            0,
                            0,
                            "linear",
                            "${ojos14}",
                            '174px',
                            '174px'
                        ],
                        [
                            "eid9370",
                            "top",
                            0,
                            0,
                            "linear",
                            "${ojos13}",
                            '127px',
                            '127px'
                        ],
                        [
                            "eid9876",
                            "top",
                            0,
                            0,
                            "linear",
                            "${ojos22}",
                            '22px',
                            '22px'
                        ],
                        [
                            "eid9181",
                            "left",
                            0,
                            0,
                            "linear",
                            "${ojos11}",
                            '319px',
                            '319px'
                        ],
                        [
                            "eid8211",
                            "left",
                            0,
                            0,
                            "linear",
                            "${ojos4}",
                            '116px',
                            '116px'
                        ],
                        [
                            "eid8316",
                            "left",
                            0,
                            0,
                            "linear",
                            "${ojos6}",
                            '216px',
                            '216px'
                        ],
                        [
                            "eid8217",
                            "top",
                            0,
                            0,
                            "linear",
                            "${ojos5}",
                            '21px',
                            '21px'
                        ],
                        [
                            "eid8656",
                            "left",
                            0,
                            0,
                            "linear",
                            "${ojos8}",
                            '287px',
                            '287px'
                        ],
                        [
                            "eid7992",
                            "left",
                            0,
                            0,
                            "linear",
                            "${ojos2}",
                            '82px',
                            '82px'
                        ],
                        [
                            "eid9877",
                            "left",
                            0,
                            0,
                            "linear",
                            "${ojos22}",
                            '255px',
                            '255px'
                        ],
                        [
                            "eid9147",
                            "left",
                            0,
                            0,
                            "linear",
                            "${ojos10}",
                            '187px',
                            '187px'
                        ],
                        [
                            "eid9838",
                            "left",
                            0,
                            0,
                            "linear",
                            "${ojos21}",
                            '353px',
                            '353px'
                        ],
                        [
                            "eid9700",
                            "left",
                            0,
                            0,
                            "linear",
                            "${ojos18}",
                            '387px',
                            '387px'
                        ],
                        [
                            "eid9833",
                            "left",
                            0,
                            0,
                            "linear",
                            "${ojos20}",
                            '218px',
                            '218px'
                        ],
                        [
                            "eid8218",
                            "left",
                            0,
                            0,
                            "linear",
                            "${ojos5}",
                            '120px',
                            '120px'
                        ],
                        [
                            "eid8839",
                            "left",
                            0,
                            0,
                            "linear",
                            "${ojos9}",
                            '283px',
                            '283px'
                        ],
                        [
                            "eid8647",
                            "top",
                            0,
                            0,
                            "linear",
                            "${ojos7}",
                            '173px',
                            '173px'
                        ],
                        [
                            "eid9145",
                            "top",
                            0,
                            0,
                            "linear",
                            "${ojos10}",
                            '18px',
                            '18px'
                        ],
                        [
                            "eid9701",
                            "top",
                            0,
                            0,
                            "linear",
                            "${ojos18}",
                            '125px',
                            '125px'
                        ],
                        [
                            "eid8314",
                            "top",
                            0,
                            0,
                            "linear",
                            "${ojos6}",
                            '173px',
                            '173px'
                        ],
                        [
                            "eid9369",
                            "left",
                            0,
                            0,
                            "linear",
                            "${ojos13}",
                            '319px',
                            '319px'
                        ],
                        [
                            "eid9697",
                            "left",
                            0,
                            0,
                            "linear",
                            "${ojos17}",
                            '251px',
                            '251px'
                        ]
                    ]
                }
            },
            "ojos1": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            transform: [[], [], [], ['1', '0']],
                            id: 'ojos2',
                            type: 'image',
                            rect: ['0px', '0px', '40px', '24px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/ojos2.png', '0px', '0px']
                        },
                        {
                            rect: ['7px', '5px', '26px', '3px', 'auto', 'auto'],
                            id: 'cejas1',
                            type: 'image',
                            fill: ['rgba(0,0,0,0)', 'images/cejas.svg', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            rect: [null, null, '40px', '24px']
                        }
                    }
                },
                timeline: {
                    duration: 3000,
                    autoPlay: true,
                    data: [
                        [
                            "eid7898",
                            "scaleY",
                            0,
                            250,
                            "linear",
                            "${ojos2}",
                            '1',
                            '0'
                        ],
                        [
                            "eid7900",
                            "scaleY",
                            250,
                            250,
                            "linear",
                            "${ojos2}",
                            '0',
                            '1'
                        ]
                    ]
                }
            },
            "ojos2": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'ojos12',
                            type: 'image',
                            rect: ['0px', '0px', '40px', '24px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/ojos1.png', '0px', '0px']
                        },
                        {
                            id: 'cejas2',
                            type: 'image',
                            rect: ['7px', '5px', '27px', '3px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/cejas2.svg', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            isStage: 'true',
                            rect: [undefined, undefined, '40px', '24px']
                        }
                    }
                },
                timeline: {
                    duration: 2000,
                    autoPlay: true,
                    data: [
                        [
                            "eid7995",
                            "scaleY",
                            500,
                            250,
                            "linear",
                            "${ojos12}",
                            '1',
                            '0'
                        ],
                        [
                            "eid7997",
                            "scaleY",
                            750,
                            250,
                            "linear",
                            "${ojos12}",
                            '0',
                            '1'
                        ]
                    ]
                }
            },
            "ojos16": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'ojos16',
                            type: 'image',
                            rect: ['0px', '0px', '40px', '24px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/ojos1.png', '0px', '0px']
                        },
                        {
                            id: 'cejas4',
                            type: 'image',
                            rect: ['6px', '6px', '27px', '3px', 'auto', 'auto'],
                            fill: ['rgba(0,0,0,0)', 'images/cejas4.svg', '0px', '0px']
                        }
                    ],
                    style: {
                        '${symbolSelector}': {
                            isStage: 'true',
                            rect: [undefined, undefined, '40px', '24px']
                        }
                    }
                },
                timeline: {
                    duration: 3000,
                    autoPlay: true,
                    data: [
                        [
                            "eid9539",
                            "scaleY",
                            500,
                            250,
                            "linear",
                            "${ojos16}",
                            '1',
                            '0'
                        ],
                        [
                            "eid9541",
                            "scaleY",
                            750,
                            250,
                            "linear",
                            "${ojos16}",
                            '0',
                            '1'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("menu_3_edgeActions.js");
})("EDGE-3421724");
