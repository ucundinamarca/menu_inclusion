/***********************
* Acciones de composición de Adobe Edge Animate
*
* Editar este archivo con precaución, teniendo cuidado de conservar 
* las firmas de función y los comentarios que comienzan con "Edge" para mantener la 
* capacidad de interactuar con estas acciones en Adobe Edge Animate
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // los alias más comunes para las clases de Edge

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
      Symbol.bindElementAction(compId, symbolName, "document", "compositionReady", function(sym, e) {
         yepnope({
         	nope:[
         		'css/udec.css',
               'js/jquery-3.3.1.min.js',
               'js/ivo.js',
               'js/TweenMax.min.js',
               'js/howler.min.js',
               'js/main.js'
            ],
            complete: init
         }); // end of yepnope
         function init() {
         	main(sym);
         }
         

      });
      //Edge binding end

   })("stage");
   //Edge symbol end:'stage'

   //=========================================================
   
   //Edge symbol: 'hojas'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 4500, function(sym, e) {
         sym.play(0);

      });
      //Edge binding end

   })("hojas");
   //Edge symbol end:'hojas'

   //=========================================================
   
   //Edge symbol: 'avatares'
   (function(symbolName) {   
   
   })("avatares");
   //Edge symbol end:'avatares'

   //=========================================================
   
   //Edge symbol: 'ojos1'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3000, function(sym, e) {
         sym.play(0);

      });
      //Edge binding end

   })("ojos1");
   //Edge symbol end:'ojos1'

   //=========================================================
   
   //Edge symbol: 'ojos2'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 2000, function(sym, e) {
         sym.play(0);

      });
      //Edge binding end

   })("ojos2");
   //Edge symbol end:'ojos2'

   //=========================================================
   
   //Edge symbol: 'ojos16'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3000, function(sym, e) {
         sym.play(0);

      });
      //Edge binding end

   })("ojos16");
   //Edge symbol end:'ojos16'

})(window.jQuery || AdobeEdge.$, AdobeEdge, "EDGE-3421724");